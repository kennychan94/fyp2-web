<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('event/add','EventController@index');
Route::post('event/add','EventController@add');
Route::get('event/edit','EventController@edit');
Route::post('event/edit','EventController@edit');
Route::get('notification/add', 'NotificationController@index');
Route::get('notification/add', 'NotificationController@add');