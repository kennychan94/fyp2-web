@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                </div>

                <div class="panel-body">
                    Welcome to UTAR event management application!
                </div>

                <div class="panel-body">
                    Choose your task at the Menu bar left side!
                </div>
                {{-- <button onclick="location.href = '{{ url('event/add')}}';" class="btn btn-primary" type="button">Add Event</button> --}}
            </div>
        </div>
    </div>
</div>
@endsection
