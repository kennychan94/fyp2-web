@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Event Registration <small>Please key in your event details.</small></h2>
                            <div class="clearfix"></div>
                        </div>

                        <div class="x_content">
                            <br/>

                            
                            <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="event-name">Event Name <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="text" name="event-name" id="event-name" required="required" class="form-control col-md-7 col-xs-12" value="UTAR BALL">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="event-description">Event Description <span class="required">*</span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea type="text" id="event-description" name="event-description" required="required" class="form-control col-md-7 col-xs-12" value="">The UTAR Ball 2015 organised by the Student Representative Council (SRC) Kampar Campus 2014/2015 was held at Kinta Riverfront Hotel, Ipoh on 12 July 2015.
Themed “Glamour Night”, the annual UTAR Ball was held for the first time in Ipoh and attended by over 250 students from both Kampar Campus and Sungai Long Campus.
Thrilled at the encouraging turnout of participants, Organising Chairperson Tam Man Wai expressed her gratitude to all who have travelled to the limestone city for such a memorable event. “The UTAR Ball provides an opportunity for students to know one another and embrace themselves as UTARians,” enthused the international student who hails from Hong Kong. “Every single one of us is special and unique, like a shining star in the night. Let’s make this UTAR Ball a magical, memorable and unforgettable moment,” she continued.</textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="event-date">Date *<span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="date" id="event-date" name="event-date" class="form-control active col-md-7 col-xs-12" value="2017-09-08">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="event-time">Time *<span class="required"></span>
                                    </label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input type="Time" id="event-date" name="event-time" class="form-control active col-md-7 col-xs-12" value="20:00:00">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="event-venue" class="control-label col-md-3 col-sm-3 col-xs-12">Venue <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                      <input id="event-venue" class="form-control col-md-7 col-xs-12" type="text" name="event-venue" value="UTAR MPH">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="event-price" class="control-label col-md-3 col-sm-3 col-xs-12">Ticket Price (MYR) <span class="required">*</span></label>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="event-price" class="form-control col-md-7 col-xs-12" type="text" name="event-price" value="10">
                                    </div>
                                </div>

                                {{-- <div class="form-group">
                                    <label for="event-photo" class="control-label col-md-3 col-sm-3 col-xs-12">Event Photo<span class="required">*</span></label> 
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type='file' onchange="readURL(this);" />
                                    <img id="event-photo" name="event-photo[]" src="#" alt="your image" multiple accept="image/x-png"/>
                                    </div>
                                </div> --}}

                                <div class="ln_solid"></div>

                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                    <button class="btn btn-primary" type="button">Cancel</button>
                                    <button class="btn btn-primary" type="reset">Reset</button>
                                    </div>
                                </div>

                    </form>

                        </div>
                    </div>
        </div>

    </div>





</div>
@endsection
@section('script')
<script>
$(document).ready(checkEitherOne(null));

function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

                reader.onload = function (e) {
                    $('#event-photo')
                        .attr('src', e.target.result)
                        .width("192px")
                        .height("192px");
                };
                reader.readAsDataURL(input.files[0]);
        }
    }
    </script>
@endsection
