@extends('layouts.template')

@section('css')
<!-- Select2 -->
<link href="{{ asset('css/select2.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/daterangepicker.css')}}" rel="stylesheet"/>
<link href="{{ asset('css/bootstrap-timepicker.min.css')}}" rel="stylesheet"/>


@endsection

@section('content')
<div class="">
    <div class="page-title">
      	<div class="title_left">
        	<h3></h3>
      	</div>
	</div>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-xs-12">
            <div class="x_panel">
<!--               	<div class="x_title">
                	<div class="clearfix"></div>
              	</div> -->
              	<div class="x_content">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="x_title">
                                    <h2>New Notification</h2>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">

                                <br/>
                                <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post">

                                {{ csrf_field() }}
{{--                                     <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="targets">Targets<span class="required"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" id="targets" name="targets" required="required" class="form-control col-md-7 col-xs-12" value="Everyone">
                                        </div>
                                    </div> --}}

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notification_name">Notification Name (100 max) * <span class="required"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="text" id="notification_name" name="notification_name" maxlength="100" required="required" class=" form-control col-md-7 col-xs-12" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="message" class="control-label col-md-3 col-sm-3 col-xs-12">Message (100 max) * </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <textarea id="message" class="form-control" name="message" required="required" data-parsley-trigger="keyup" data-parsley-minlength="20" maxlength="100" data-parsley-maxlength="100"
                                          data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                                          data-parsley-validation-threshold="10"></textarea>
                                        </div>
                                    </div>

{{--                                     <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="link">Link<span class="required"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="url" id="link" name="link" placeholder="e.g. http://abc.com" class="form-control col-md-7 col-xs-12" value="">
                                        </div>
                                    </div> --}}

                                    {{-- <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date">Date *<span class="required"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="text" id="date" name="date" class="form-control active col-md-7 col-xs-12" value="">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="time">Time *<span class="required"></span>
                                        </label>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                          <input type="text" id="time" name="time" class="form-control active col-md-7 col-xs-12" value="">
                                        </div>
                                    </div>
 --}}
                                    <div class="ln_solid"></div>
                                    <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                    </div>
                                  </div>

                                </form>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
           	</div>
        </div>
	</div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/transition.js')}}"></script>
<script src="{{ asset('js/collapse.js')}}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{ asset('js/bootstrap-timepicker.min.js')}}"></script>

        <script type="text/javascript">
            $(function() {
                $('input[name="date"]').daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true
                });

                $('#time').timepicker();

                // $('#time').datetimepicker({
                //     format: 'LT',
                //     minuteStep: 15
                // });


                // $('#time').timepicker({ 'step': 15 });
            });


            // $(function () {
            //     $('#date').datetimepicker();
            //     $('#time').timepicker({ 'step': 15 });
            // });

            

        </script>



@endsection
