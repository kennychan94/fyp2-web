<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Request;
use App\Event;
use App\Attendee;
use OneSignal;
class EventController extends Controller
{
    //

    public function index(){
    	return view ('event.add');
    }

    public function add(){

            $player_ids = array();
            $users = Attendee::where('player_id','!=',null)->get();
	    	$event = new Event;
	    	$event->eventName = Request::input('event-name');
	    	$event->eventDesc = Request::input('event-description');
	    	$event->eventDate = Request::input('event-date');
	    	$event->eventTime = Request::input('event-time');
	    	$event->eventPrice = Request::input('event-price');
	    	$event->save();

            if ($users->first() != null){
                foreach ($users as $user){
                    array_push($player_ids, $user->player_id);
                }

                $response = OneSignal::postNotification([
                "include_player_ids"     => [implode(",",$player_ids)],
                "contents"              => ["en" => "New event coming"],
                "headings"              => ["en" => $event->eventName],
                ],'5b0f3516-0c45-4827-af75-b0316faf1ae4');
            
            }

	    	return redirect('home');
    }

    public function getAllEvents(){
    	$events = Event::orderby("id",'desc')->get();
    	return response()->json($events,200);
    }

    public function edit(){
        return view ('event.edit');
    }



}
