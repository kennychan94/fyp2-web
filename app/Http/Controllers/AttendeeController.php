<?php

namespace App\Http\Controllers;

// use Illuminate\Http\Request;
use Request;
use App\Attendee;

class AttendeeController extends Controller
{
    //
    public function register(){
    	$new = new Attendee;
    	$new->name = Request::input('name');
    	$new->email = Request::input('email');
    	$new->password = "123123";
    	$new->player_id = Request::input('player_id');
    	$new->save();
    	return response()->json('registered',200); 
    }

    public function login(){
    	$user = Attendee::where('email',Request::input('email'))->first();
    	if ($user != null){
    		if($user->password == Request::input('password')){
                $user->player_id = Request::input('player_id');
                $user->save();
    			return response()->json("logged in",200);
    		}
    	}
    	return response()->json("no such email",200);
    }
}
