<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;




use Validator;
use OneSignal;
use App\User;
use App\Attendee;

class NotificationController extends Controller
{
    //

    public function index(){
    	return view('notification.send');
    }

    public function add()
    {
    	$players_ids = array();
        $users = Attendee::where('player_id','!=',null)->get();

        if ($users->first() != null){
        foreach ($users as $user){
            array_push($player_ids, $user->player_id);
        }

        $response = OneSignal::postNotification([
        "include_player_ids"     => [implode(",",$player_ids)],
        "contents"              => ["en" => Request::input('message')],
        "headings"              => ["en" => Request::input('notification_name')],
        ],'5b0f3516-0c45-4827-af75-b0316faf1ae4');
	    
	    }

		return redirect('home');      
    }

}
